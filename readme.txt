项目介绍
    renren-generator：代码生成器
    renren-admin：权限系统

项目启动日志
    1.启动前端代码：进入根目录，npm install     npm run serve
    2.初始化数据库：新建数据库renren_security，在renren-admin/db找到对应数据库sql，执行
    3.修改配置文件application.yml和dev（主要是数据库连接配置）
    4.启动renren-admin

    renren-admin访问路径：http://localhost:8080/renren-admin
    swagger文档路径：http://localhost:8080/renren-admin/doc.html
    再启动前端项目，前端地址：https://gitee.com/renrenio/renren-ui
    账号密码：admin/admin
